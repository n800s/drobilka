LABELS = (
	{
		'class': 'car_dirt',
		'val': 1,
		'segnet_val': 1,
		'color': (165, 100, 3, 255),
	},
	{
		'class': 'car_emty',
		'val': 2,
		'segnet_val': 2,
		'color': (4, 215, 208, 255),
	},
	{
		'class': 'loader_dirt',
		'val': 3,
		'segnet_val': 3,
		'color': (251, 235, 60, 255),
	},
	{
		'class': 'loader_emty',
		'val': 4,
		'segnet_val': 4,
		'color': (30, 96, 250, 255),
	},
)

