import os, sys, shutil, tempfile, glob
import subprocess

def predict_video(ifname, ofname):
	ifname = os.path.abspath(ifname)
	ofname = os.path.abspath(ofname)
	print(ifname, '->', ofname, 'started')
	cpath = os.path.dirname(__file__)
	odname = tempfile.mkdtemp('', 'drobilka_inf_')
	try:
		owd = os.getcwd()
		os.chdir(cpath)
		try:
			cmd = ['python3', 'predict.py', '--conf', 'config.json', '--input', ifname, '--output', odname]
			print('Executing', subprocess.list2cmdline(cmd))
			subprocess.check_output(cmd)
			for fn in glob.glob(os.path.join(odname, '*.mp4')):
				os.rename(fn, ofname)
			for fn in glob.glob(os.path.join(odname, '*.json')):
				os.rename(fn, ofname + '.json')
		finally:
			os.chdir(owd)
	finally:
		shutil.rmtree(odname)
	print(ifname, '->', ofname, 'finished')
	return {}

def predict_photo(ifname, ofname):
	ifname = os.path.abspath(ifname)
	ofname = os.path.abspath(ofname)
	print(ifname, '->', ofname, 'started')
	cpath = os.path.dirname(__file__)
	odname = tempfile.mkdtemp('', 'drobilka_inf_')
	try:
		owd = os.getcwd()
		os.chdir(cpath)
		try:
			cmd = ['python3', 'predict.py', '--conf', 'config.json', '--input', ifname, '--output', odname]
			print('Executing', subprocess.list2cmdline(cmd))
			subprocess.check_output(cmd)
			for fn in glob.glob(os.path.join(odname, '*.jpg')):
				print(fn, 'renamed to', ofname)
				os.rename(fn, ofname)
			for fn in glob.glob(os.path.join(odname, '*.json')):
				os.rename(fn, ofname + '.json')
		finally:
			os.chdir(owd)
	finally:
		shutil.rmtree(odname)
	print(ifname, '->', ofname, 'finished')
	return {}
