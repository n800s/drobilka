#! /usr/bin/env python

import os
import argparse
import json
import glob
import cv2
from utils.utils import get_yolo_boxes, makedirs
from utils.bbox import draw_boxes
from tensorflow.keras.models import load_model
from tqdm import tqdm
import numpy as np

import tensorflow as tf
print('TF:', tf.__version__)
if int(tf.__version__.split('.')[0]) >= 2:

	from tensorflow.compat.v1 import ConfigProto
	from tensorflow.compat.v1 import InteractiveSession
else:
	from tensorflow import ConfigProto
	from tensorflow import InteractiveSession
config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

# process boxes and put data into json_data object and json_boxes list
def boxes2json(boxes, json_data, json_boxes):
	for b in boxes:
		if b.label_str:
			json_box = {'label': b.label_str, 'box': [b.xmin, b.ymin, b.xmax, b.ymax], 'width': b.xmax-b.xmin, 'height': b.ymax-b.ymin}
			json_box['area'] = json_box['height'] * json_box['width']
			lcount = len(list(filter(lambda v: v>0 , b.classes)))
			if lcount > 1:
				json_box['label_count'] = lcount
			json_boxes.append(json_box)
			label_str = b.label_str.split(' ')[0]
			json_data['label_count'][label_str] = json_data['label_count'].get(label_str, 0) + 1


def _main_(args):
	config_path  = args.conf
	input_path   = args.input
	output_path  = args.output

	with open(config_path) as config_buffer:
		config = json.load(config_buffer)

	makedirs(output_path)

	###############################
	#   Set some parameter
	###############################	
	net_h, net_w = 416, 416 # a multiple of 32, the smaller the faster
	obj_thresh, nms_thresh = 0.5, 0.45

	###############################
	#   Load the model
	###############################
	os.environ['CUDA_VISIBLE_DEVICES'] = config['train']['gpus']
	infer_model = load_model(config['train']['saved_weights_name'])

	###############################
	#   Predict bounding boxes
	###############################
	if 'webcam' in input_path: # do detection on the first webcam
		video_reader = cv2.VideoCapture(0)

		# the main loop
		batch_size  = 1
		images	  = []
		while True:
			ret_val, image = video_reader.read()
			if ret_val == True: images += [image]

			if (len(images)==batch_size) or (ret_val==False and len(images)>0):
				batch_boxes = get_yolo_boxes(infer_model, images, net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)

				for i in range(len(images)):
					draw_boxes(images[i], batch_boxes[i], sorted(config['model']['labels']), obj_thresh)
					cv2.imshow('video with bboxes', images[i])
				images = []
			if cv2.waitKey(1) == 27:
				break  # esc to quit
		cv2.destroyAllWindows()
	elif os.path.splitext(input_path)[-1] == '.mp4': # do detection on a video
		video_out = os.path.join(output_path, os.path.basename(input_path))
		video_reader = cv2.VideoCapture(input_path)

		nb_frames = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
		frame_h = int(video_reader.get(cv2.CAP_PROP_FRAME_HEIGHT))
		frame_w = int(video_reader.get(cv2.CAP_PROP_FRAME_WIDTH))
		fps = int(video_reader.get(cv2.CAP_PROP_FPS))

		video_writer = cv2.VideoWriter(video_out, cv2.VideoWriter_fourcc(*'mp4v'), fps, (frame_w, frame_h))
		# the main loop
		batch_size  = 1
		images	  = []
		start_point = 0 #%
		show_window = False
		json_data = {'frames': [], 'label_count': {}}
#		for i in tqdm(range(nb_frames)):
		for i in range(nb_frames):
			_, image = video_reader.read()

			if (float(i+1)/nb_frames) > start_point/100.:
				images += [image]

				if (i%batch_size == 0) or (i == (nb_frames-1) and len(images) > 0):
					json_frame = {'frame': i}
					# predict the bounding boxes
					batch_boxes = get_yolo_boxes(infer_model, images, net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)

					json_boxes = []
					for j in range(len(images)):
						# draw bounding boxes on the image using labels
						draw_boxes(images[j], batch_boxes[j], sorted(config['model']['labels']), obj_thresh, quiet=True)

						boxes2json(batch_boxes[j], json_data, json_boxes)

						# write result to the output video
						video_writer.write(images[j])
					if json_boxes:
						json_frame['boxes'] = json_boxes
						json_frame['box_count'] = len(json_boxes)
						json_data['frames'].append(json_frame)
					images = []

		json.dump(json_data, open(video_out + '.json', 'w'), indent=2)
		video_reader.release()
		video_writer.release()
	else: # do detection on an image or a set of images

		json_data = {'frames': [], 'label_count': {}}

		image = cv2.imread(input_path)

		print('Path:%s' % input_path)
		if not image is None:

			# predict the bounding boxes
			batch_boxes = get_yolo_boxes(infer_model, [image], net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)

			# draw bounding boxes on the image using labels adding some more data to'em
			draw_boxes(image, batch_boxes[0], sorted(config['model']['labels']), obj_thresh, quiet=True)

			json_boxes = []
			boxes2json(batch_boxes[0], json_data, json_boxes)

			json_frame = {
				'frame': 0,
				'boxes': json_boxes,
				'box_count': len(json_boxes),
			}
			json_data['frames'].append(json_frame)

			# write the image with bounding boxes to file
			ofname = os.path.join(output_path, os.path.basename(input_path))
			cv2.imwrite(ofname, np.uint8(image))
			json.dump(json_data, open(ofname + '.json', 'w'), indent=2)

if __name__ == '__main__':
	argparser = argparse.ArgumentParser(description='Predict with a trained yolo model')
	argparser.add_argument('-c', '--conf', help='path to configuration file')
	argparser.add_argument('-i', '--input', help='path to an image, a directory of images, a video, or webcam')
	argparser.add_argument('-o', '--output', default='output/', help='path to output directory')

	args = argparser.parse_args()
	_main_(args)
