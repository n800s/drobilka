#!/usr/bin/env python3

import os, sys, json, glob
from xml.dom import minidom

config = json.load(open('config.json'))

imagedir = config['train']['train_image_folder']
xmldir = config['train']['train_annot_folder']

for xml in glob.glob(os.path.join(xmldir, '*.xml')):
	doc = minidom.parse(open(xml))
	fname = doc.getElementsByTagName("filename")[0].childNodes[0].data
	if not os.path.exists(os.path.join(imagedir, fname)):
		print(fname, 'not found')
		bad_fname = os.path.join(os.path.dirname(xml), 'bad', os.path.basename(xml))
		if not os.path.exists(os.path.join(os.path.dirname(xml), 'bad')):
			os.makedirs(os.path.exists(os.path.join(os.path.dirname(xml), 'bad')))
		os.move(xml, bad_fname)

