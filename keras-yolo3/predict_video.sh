

rm output/vids/*.{json,avi,mp4}
mkdir -p output/vids

export TF_CPP_MIN_LOG_LEVEL=1
marker='mp4'
N=50
#marker='198.4'
rm predict_video.log

for pth in test_vids
do
#F002000000000000104208008000_1522960826.4.mp4
find "$pth/." -type f -name "*.mp4" |grep $marker | sort -R | tail -n${N} | while read rfname
do
#echo $rfname
python3 -u predict.py  --conf config.json --input "$rfname" --output output/vids &>>predict_video.log
if [ $? != 0 ]
then
	echo err
	break
fi

#break

done

done




