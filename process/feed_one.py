#!/usr/bin/env python

import redis, os, sys, json

r = redis.Redis()

request = json.dumps({
    "type": "yolo3",
    "callback": "http://localhost/",
    "ftype": "video",
#    "uri": "http://crusher.infohit.ru/api/get/video/4724/"
    "uri": "file://" + os.path.abspath(sys.argv[1]),
})

r.lpush('l.dr_request', request)
