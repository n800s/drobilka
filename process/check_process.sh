#!/bin/sh

cd `dirname $0`

if ps -eo pid,command |grep python|grep -E 'drobilka_process_t'|grep -v grep;
then
        echo "$(date '+%Y %b %d %H:%M')|| drobilka_process_t is still running." >> check_process.log
else
        echo "$(date '+%Y %b %d %H:%M')|| drobilka_process_t is stopped." >> check_process.log
		./drobilka_process_t.sh
        echo "$(date '+%Y %b %d %H:%M')|| drobilka_process_t is started." >> check_process.log
fi


