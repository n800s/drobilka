#!/usr/bin/env python3

import redis, time, json, sys, os, traceback, tempfile, shutil
import argparse
import wget
import subprocess
import uuid

import tensorflow as tf
print('TF:', tf.__version__)
if int(tf.__version__.split('.')[0]) >= 2:

	from tensorflow.compat.v1 import ConfigProto
	from tensorflow.compat.v1 import InteractiveSession
else:
	from tensorflow import ConfigProto
	from tensorflow import InteractiveSession
config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

DONE_MAX = 100

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../keras-yolo3')))
from predict_inf import predict_video, predict_photo

def dbprint(text):
	print(time.strftime('%Y-%m-%d %H:%M:%S'), text, file=sys.stdout)
	sys.stdout.flush()

TMP_PATH = os.path.expanduser('~/tmp/drobilka')
if not os.path.exists(TMP_PATH):
	os.makedirs(TMP_PATH)

ap = argparse.ArgumentParser()
ap.add_argument('--redis_host', type=str, default='localhost')
args = vars(ap.parse_args())

r = redis.Redis(host=args['redis_host'])

dbprint('Started')

while True:

	try:
		req = r.brpop('l.dr_request', 0)[1].decode("utf-8")
		req = json.loads(req)
		try:
			ftype = req['ftype']
			uid = str(uuid.uuid4())
			if ftype == 'photo':
				fname = uid + '.input.jpg'
			elif ftype == 'video':
				fname = uid + 'input.mp4'
			else:
				raise Exception('Unknown ftype %s' % ftype)
			debug = os.path.exists(os.path.join(os.path.dirname(__file__), 'debug'))
			if debug:
				dbprint('request: %s' % json.dumps(req, indent=4))
			req_debug = req.get('get_debug', False)
			tmpdir = tempfile.mkdtemp(prefix='wget_', dir=TMP_PATH)
			fname = os.path.join(tmpdir, fname)
			fname = wget.download(req['uri'], out=fname, bar=None)
			try:
				dbprint('Processing %s' % fname)
				bname = os.path.splitext(fname)[0]
				if ftype == 'photo':
					ofname = os.path.join(tmpdir, uid + '.output.jpg')
					predict_photo(fname, ofname)
				else:
					ofname = os.path.join(tmpdir, uid + '.output.mp4')
					predict_video(fname, ofname)
				jofname = ofname + '.json'
				data = {
					'filename': os.path.basename(ofname),
					'data': json.load(open(jofname)) if os.path.exists(jofname) else [],
				}
				#r.lpush('l.dr_done', json.dumps({'uri': req['uri'], 'data': data}))
				#req['data'] = data
				req.update(data)
				done_count = r.llen('l.dr_done')
				for i in range(done_count-DONE_MAX):
					r.rpop('l.dr_done')

				cmd = ['curl', '-F', 'file=@' + ofname + ';filename=' + os.path.basename(ofname)]
				cmd += ['-F', 'file=@' + jofname + ';filename=' + os.path.basename(jofname)]
				cmd += [req['callback']]
				dbprint('Executing: %s' % ' '.join(cmd))
				subprocess.check_output(cmd)
				if not debug:
					os.unlink(ofname)
					os.unlink(jofname)
				r.lpush('l.dr_done', json.dumps(req))
			finally:
				try:
					if not debug:
						os.unlink(fname)
#						shutil.rmtree(tmpdir)
					pass
				except Exception as e:
					print(e)
		except KeyboardInterrupt:
			raise
		except:
			req['error'] = traceback.format_exc()
			dbprint(req['error'])
			r.lpush('l.dr_errors', json.dumps(req))
	except KeyboardInterrupt:
		raise
	except Exception as e:
		dbprint('%s' % e)
