#!/bin/bash

export PATH=${PATH}:~/.local/bin:/opt/cuda/bin:~/bin
export LD_LIBRARY_PATH=${HOME}/.local/lib
export CUDA_VISIBLE_DEVICES=0
export NVIDIA_VISIBLE_DEVICES=0
export TF_CPP_MIN_LOG_LEVEL=2
export TF_FORCE_GPU_ALLOW_GROWTH=true

cd `dirname $0`
python3 -u ./drobilka_process_t.py &> drobilka_process_t.log &
echo $?
